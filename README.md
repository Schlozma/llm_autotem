# Large Language Models (LLMs) for automated microscope control

This is an implementation of the LLM_autoTEM prototype developed during the [on-site LLM Hackathon for Applications in Materials and Chemistry in Berlin](https://events.fairmat-nfdi.eu/event/17/) as part of the global [LLM Hackathon for Applications in Materials and Chemistry](https://www.eventbrite.com/e/llm-hackathon-for-applications-in-materials-and-chemistry-tickets-868303598437)  by *Marcel Schloz and Jose Cojal Gonzalez*.

<p align="center">
 <img src="./misc/autoTEM_idea.png" alt="Drawing", width=40%>
</p>

The implementation is tailored for electron microscopy to enable (partially) autonomous operation of the instrument through the use of a LLM (Llama3-70b). Here, we try to solve one of the big problems in experimental materials science research - the high complexity and cost of ownership of state-of-the-art electron microscopes allows only a few labs staffed with expert operators who have undergone extensive microcope-specific training to run them. Maximizing these instrument’s scientific output per time as well as democratizing access to them calls for improving their user interface in analogy to how modern chatbots have recently started to enable anybody to write complex computer programs. Our prototype thus contributes to the current development of artificial intelligence and machine learning approaches to address [some of the most challenging problems in materials science](https://arxiv.org/pdf/2402.10932).


## Prototype Description

For the current implementation of our prototype we focus on the task of performing "simulated" microscopy experiments using the [abTEM simulation tool](https://abtem.readthedocs.io/en/latest/intro.html) and use materials files from the [materials project](https://next-gen.materialsproject.org/). The following Figure illustrates such a simulated experiment, where a simulated double-walled carbon nanotube is imaged with various microscopy techniques.

<p align="center">
 <img src="./misc/abTEM_example.png" alt="Drawing", width=60%>
</p>

Our prototype is a LLM-powered agent that interacts with the simulation tool (microscope interface) and thus follows a [schema](https://kjablonka.com/blog/posts/building_an_llm_agent) proposed by Kevin Jablonka. A demonstration of our LLM_autoTEM agent for the generation of a high resolution HAADF-STEM image of a MoS$_2$ monolayer can be found on [Youtube](https://youtu.be/aHT3Ec0IB-0).

### System Requirement

+ software
    + mp-api
    + ase
    + abtem
    + langchain
    + json_repair
    + robocrys




