from langchain import hub
from langchain.agents import create_react_agent, AgentExecutor, Tool
from langchain_community.llms import HuggingFaceEndpoint
from langchain_community.chat_models.huggingface import ChatHuggingFace
from pdf_database_rag import generate_pdf_retriever_tool
from abtem_functions import createAbtemTools
import os


#Create an abTEM tool
haadf_tool = createAbtemTools("haadf")
bf_tool = createAbtemTools("bf")
#Create a retriever-tool for a pdf database
database_path = 'YOUR_DATABASE_PATH'
pdf_retriever_tool = generate_pdf_retriever_tool(database_path)

#List all the tools in a toolbox
toolbox = [haadf_tool, bf_tool, pdf_retriever_tool]

#  Initialize the huggingface chat llm 
# (a list of options can be found here: https://huggingface.co/chat/models )
repo_id = "mistralai/Mixtral-8x7B-Instruct-v0.1"
HUGGINGFACEHUB_API_TOKEN = "YOUR_HF_API_TOKEN"
os.environ["HUGGINGFACEHUB_API_TOKEN"] = HUGGINGFACEHUB_API_TOKEN
hf_llm = HuggingFaceEndpoint(repo_id=repo_id, temperature=0.01, huggingfacehub_api_token=HUGGINGFACEHUB_API_TOKEN)
chat_hf_llm = ChatHuggingFace(llm=hf_llm)

#Create the ReAct prompt template
react_prompt = hub.pull("hwchase17/react")

#Create the agent
llmicroscopilot = create_react_agent(tools=toolbox, llm=chat_hf_llm, prompt=react_prompt)
#Query the agent
query = {"input": "Predict some experimental parameters for high angle annular dark field (HAADF) scanning transmission electron microscope (STEM) of a monolayer MoS2 sample (material_id = mp-2815) and generate an microscope image."}  

llmicroscopilot_executor = AgentExecutor(agent=llmicroscopilot, tools=toolbox, verbose=True, handle_parsing_errors=True, max_iterations=3)
output = llmicroscopilot_executor.invoke(query)
