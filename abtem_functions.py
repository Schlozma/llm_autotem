from langchain.agents import Tool
from langchain_core.pydantic_v1 import BaseModel, Field
from langchain_core.tools import StructuredTool

import abtem
from abtem import *
from ase.io import read
from ase.build import surface

from pymatgen.ext.matproj import MPRester
from pymatgen.core.structure import Structure


# Set up your API key for the Materials Project
MP_API_KEY = 'YOUR_MATERIALS_PROJECT_API_KEY'
mpr = MPRester(MP_API_KEY)


#Perform the structure retrieval from the Materials Project
def get_cif_structure(material_id):
    """Fetches the structure of a material by ID and returns a CIF string."""
    structure = mpr.get_structure_by_material_id(material_id)
    cif_data = structure.to(fmt="cif")
    cif_file_like = io.StringIO(cif_data)
    return cif_file_like


def bf_measurement(material_id: str, energy: int, conv_angle: int, scan_step: float, inner_angle: int, outer_angle: int):
    """Generate a BF-STEM image."""
    atoms = read(get_cif_structure(material_id), format='cif')
    sample = surface(atoms, indices=(0,0,1), layers=4, periodic=True)
    sample, strain = orthogonalize_cell(sample, return_transform=True)
    sample_potential = Potential(sample, sampling=.05)
    scan = GridScan(start=[0, 0], end=[20, 20], sampling=scan_step)
    probe = probe_construction(sample)
    bf_detector = AnnularDetector(inner=inner_angle, outer=outer_angle)
    bf_scan = probe.scan(sample_potential, scan, bf_detector)
    #bf_scan.show()
    return bf_scan


def haadf_measurement(material_id: str, energy: int, conv_angle: int, scan_step: float, inner_angle: int, outer_angle: int):
    """Generate a HAADF-STEM image."""
    atoms = read(get_cif_structure(material_id), format='cif')
    sample = surface(atoms, indices=(0,0,1), layers=4, periodic=True)
    sample, strain = orthogonalize_cell(sample, return_transform=True)
    sample_potential = Potential(sample, sampling=.05)
    scan = GridScan(start=[0, 0], end=[20, 20], sampling=scan_step)
    probe = probe_construction(sample)
    haadf_detector = AnnularDetector(inner=inner_angle, outer=outer_angle)
    haadf_scan = probe.scan(sample_potential, scan, haadf_detector)
    #haadf_scan.show()
    return haadf_scan
    


class abtemInput(BaseModel):
    material_id: str = Field(description="Materials Project specific ID to fetch the material structure file.")
    energy: int = Field(description="Acceleration voltage (electron energy) used in the microscope given in the unit kV.")
    conv_angle: int = Field(description="Semi-convergence angle used in the microscope to form the convergent probe given in the unit mrad.")
    scan_step: float = Field(description="Scan step used by the scanning probe to scan the sample. Unit is given in Angstrom.")
    inner_angle: int = Field(description="Inner angle size used by the scanning transmission electron microscope detector. Unit in mrad")
    outer_angle: int = Field(description="Outer angle size used by the scanning transmission electron microscope detector. Unit in mrad")


def createAbtemTools(mode):

    if mode == "haadf":
        abtem_tool = StructuredTool.from_function(
        func=haadf_measurement,
        name="haadf_measurement",
        description="Use this tool to generate a high angle annular dark field (HAADF) scanning transmission electron microscope (STEM) image provided experimental parameters.",
        args_schema=abtemInput,
        return_direct=True,
        # coroutine= ... <- you can specify an async method if desired as well
        )

    if mode == "bf":
        abtem_tool = StructuredTool.from_function(
        func=bf_measurement,
        name="bf_measurement",
        description="Use this tool to generate a bright field (BF) scanning transmission electron microscope (STEM) image provided experimental parameters.",
        args_schema=abtemInput,
        return_direct=True,
        # coroutine= ... <- you can specify an async method if desired as well
        )

    return abtem_tool

