from langchain_community.embeddings import HuggingFaceEmbeddings
from sentence_transformers import SentenceTransformer
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import DirectoryLoader, PyMuPDFLoader
from langchain.tools.retriever import create_retriever_tool
from langchain_chroma import Chroma


MARKDOWN_SEPARATORS = [
    "\n#{1,6} ",
    "```\n",
    "\n\\*\\*\\*+\n",
    "\n---+\n",
    "\n___+\n",
    "\n\n",
    "\n",
    " ",
    "",
]


EMBEDDING_MODEL_NAME = "thenlper/gte-small"


def generate_pdf_retriever_tool(database_path):
    # Extract text from PDF files
    loader = DirectoryLoader(database_path, glob="./*.pdf", loader_cls=PyMuPDFLoader , show_progress=True)
    documents = loader.load()
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=512, chunk_overlap=24, add_start_index=True, strip_whitespace=True, separators=MARKDOWN_SEPARATORS)
    texts = text_splitter.split_documents(documents)
    # Initialize the embeddings model
    embeddings_model = HuggingFaceEmbeddings(model_name=EMBEDDING_MODEL_NAME, encode_kwargs={"normalize_embeddings": True},)
    vectordb = Chroma.from_documents(documents=texts, embedding=embeddings_model)
    pdf_retriever = vectordb.as_retriever(search_kwargs={"k": 2})
    pdf_retriever_tool = create_retriever_tool(
    retriever=pdf_retriever,
    name="pdf_database_search",
    description="Search for information about experimental microscope parameters. For any questions about scanning tunneling electron microscopy (STEM), you must use this tool!",
    )

    return pdf_retriever_tool

#retriever = generate_pdf_retriever_tool('/home/marcel/Desktop/Gitlab/LLM_autoTEM/LLMicrosCopilot/database')
#query = "What would be good experimental parameters for high resolution HAADF-STEM images?"
#output = retriever.invoke(query)
#print(output)

